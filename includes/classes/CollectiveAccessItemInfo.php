<?php
// $Id$

/**
 * @file
 * Expose generic ItemInfo functionalities to be used by specific factory classes
 */

class CollectiveAccessItemInfo {
  /**
   * Call the getItem method
   *
   * @param CollectiveAccess $ca
   *   The CollectiveAccess object to execute methods on
   * @param string $type
   *   The item type to fetch
   * @param int $item_id
   *   The unique identifier for an item, as known to the CollectiveAccess instance
   * @return
   *   array
   *     The raw data as returned by CollectiveAccess
   */
  public static function getItem(CollectiveAccess $ca, $type, $item_id, $cache = NULL) {
    if ($ca instanceof CollectiveAccess) {
      $params = array(
        'type' => $type,
        'item_id' => $item_id,
      );

      if (isset($cache)) { // override default
        $data = $ca->execute('ItemInfo', 'getItem', $params, $cache);
      }
      else { // use default
        $data = $ca->execute('ItemInfo', 'getItem', $params);
      }
      return $data;
    }
    return FALSE;
  }

  /**
   * Call the getAttributes method
   *
   * @param CollectiveAccess $ca
   *   The CollectiveAccess object to execute methods on
   * @param string $type
   *   The item type to fetch
   * @param int $item_id
   *   The unique identifier for an item, as known to the CollectiveAccess instance
   * @return
   *   array
   *     The attributes for a specific item
   */
  public static function getAttributes(CollectiveAccess $ca, $type, $item_id, $cache = NULL) {
    $attribs = FALSE;
    if ($ca instanceof CollectiveAccess) {
      $params = array(
        'type' => $type,
        'item_id' => $item_id,
      );

      if (isset($cache)) { // override default
        $data = $ca->execute('ItemInfo', 'getAttributes', $params, $cache);
      }
      else { // use default
        $data = $ca->execute('ItemInfo', 'getAttributes', $params);
      }
      if ($data) {
        // rearrange array to have attribute name as array key
        foreach ($data as $k => $v) {
          if ($v[0]) {
            $attribute_name = $v[0]['element_code'];
            $attribs[$attribute_name] = $v;
          }
        }
      }
    }
    return $attribs;
  }

  /**
   * Call the getLabels method
   *
   * @param CollectiveAccess $ca
   *   The CollectiveAccess object to execute methods on
   * @param string $type
   *   The item type to fetch
   * @param int $item_id
   *   The unique identifier for an item, as known to the CollectiveAccess instance
   * @param string $mode
   *   Can be either 'preferred', 'nonpreferred' or 'all'
   * @return
   *   array
   *     The labels for a specific item
   */
  public static function getLabels(CollectiveAccess $ca, $type, $item_id, $mode = 'all', $cache = NULL) {
    $labeldata = FALSE;
    if ($ca instanceof CollectiveAccess) {
      $params = array(
        'type' => $type,
        'item_id' => $item_id,
        'mode' => $mode
      );

      if (isset($cache)) { // override default
        $data = $ca->execute('ItemInfo', 'getLabels', $params, $cache);
      }
      else { // use default
        $data = $ca->execute('ItemInfo', 'getLabels', $params);
      }

      if ($data) {
        foreach ($data as $objectid => $labels) {
          foreach ($labels as $localeid => $label) {
            if ($label[0]) {
              $locale_name = $label[0]['locale_language'] . '_' . $label[0]['locale_country'];
              $labeldata[$locale_name] = $label;
            }
          }
        }
      }
    }
    return $labeldata;
  }

  /**
   * Call the getObjectRepresentations method
   *
   * @param CollectiveAccess $ca
   *   The CollectiveAccess object to execute methods on
   * @param int $object_id
   *   The unique identifier for an object, as known to the CollectiveAccess instance
   * @return
   *   array
   *     The raw data as returned by CollectiveAccess
   */
  public static function getObjectRepresentations(CollectiveAccess $ca, $object_id, $cache = NULL) {
    $representations = FALSE;
    if ($ca instanceof CollectiveAccess) {
      $params = array(
        'object_id' => $object_id,
      );

      if (isset($cache)) { // override default
        $data = $ca->execute('ItemInfo', 'getObjectRepresentations', $params, $cache);
      }
      else { // use default
        $data = $ca->execute('ItemInfo', 'getObjectRepresentations', $params);
      }
      return $data;
    }
    return FALSE;
  }

  /**
   * Call the getRelationships method
   *
   * @param CollectiveAccess $ca
   *   The CollectiveAccess object to execute methods on
   * @param string $type
   *   The item type to fetch
   * @param int $item_id
   *   The unique identifier for an item, as known to the CollectiveAccess instance
   * @param string $related_type
   *   Can be one of ca_objects, ca_entities, ca_places, ca_occurences, ca_collections, ca_list_items
   * @return
   *   array
   *     The relationship data for a specific item
   */
  public static function getRelationships(CollectiveAccess $ca, $type, $item_id, $related_type, $cache = NULL) {
    if ($ca instanceof CollectiveAccess) {
      $params = array(
        'type' => $type,
        'item_id' => $item_id,
        'related_type' => $related_type
      );

      if (isset($cache)) { // override default
        $data = $ca->execute('ItemInfo', 'getRelationships', $params, $cache);
      }
      else { // use default
        $data = $ca->execute('ItemInfo', 'getRelationships', $params);
      }
      return $data;
    }
  }


  /**
   * Call the getSets method
   *
   * @param CollectiveAccess $ca
   *   The CollectiveAccess object to execute methods on
   * @return
   *   array
   *     The available sets for this CollectiveAccess instance
   */
  public static function getSets(CollectiveAccess $ca, $cache = NULL) {
    if ($ca instanceof CollectiveAccess) {
      $params = array();

      if (isset($cache)) { // override default
        $data = $ca->execute('ItemInfo', 'getSets', $params, $cache);
      }
      else { // use default
        $data = $ca->execute('ItemInfo', 'getSets', $params);
      }
      return $data;
    }
    return FALSE;
  }

  /**
   * Call the getSet method
   *
   * @param CollectiveAccess $ca
   *   The CollectiveAccess object to execute methods on
   * @param int $set_id
   *   The unique identifier for a set, as known to the CollectiveAccess instance
   * @return
   *   CollectiveAccessSet
   *     An object representing the set info
   */
  public static function getSet(CollectiveAccess $ca, $set_id, $cache = NULL) {
    if ($ca instanceof CollectiveAccess) {
      $params = array(
        'set_id' => $set_id,
      );

      if (isset($cache)) { // override default
        $data = $ca->execute('ItemInfo', 'getSet', $params, $cache);
      }
      else { // use default
        $data = $ca->execute('ItemInfo', 'getSet', $params);
      }
      return $data;
    }
  }

  /**
   * Call the getSetItems method
   *
   * @param CollectiveAccess $ca
   *   The CollectiveAccess object to execute methods on
   * @param int $set_id
   *   The unique identifier for a set, as known to the CollectiveAccess instance
   * @return
   *   array
   *     A list of set items
   */
  public static function getSetItems(CollectiveAccess $ca, $set_id, $cache = NULL) {
    if ($ca instanceof CollectiveAccess) {
      $params = array(
        'set_id' => $set_id,
      );

      if (isset($cache)) { // override default
        $data = $ca->execute('ItemInfo', 'getSetItems', $params, $cache);
      }
      else { // use default
        $data = $ca->execute('ItemInfo', 'getSetItems', $params);
      }
      return $data;
    }
  }
}