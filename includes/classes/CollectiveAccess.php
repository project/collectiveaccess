<?php
// $Id$

/**
 * @file
 *
 * class wrapping around a CollectiveAccess instance
 */

class CollectiveAccess {
  private $connector;
  private $cachehandler;

  /**
   * Constructor for CollectiveAccess class
   */
  public function __construct(CollectiveAccessConnector $connector, $cachehandler = NULL) {
    $this->connector = $connector;
    if ($cachehandler) {
      $this->cachehandler = NULL;
    }
  }

  /**
   * Execute a webservice API call
   * See CollectiveAccess Service API for more information
   * (http://wiki.collectiveaccess.org/index.php?title=Web_services)
   *
   * @param string $service
   *   Specifies the CollectiveAccess service you want to call. Typically one of:
   *   Cataloguing, ItemInfo, UserContent, Search, Browse
   * @param string $method
   *   Specifies the method to be called on the selected CollectiveAccess service
   * @param array $params
   *   Provide an array of parameter values to pass to the method.
   * @param boolean $cache
   *   Specify whether a cachehandler should be used to retrieve the data
   *   from a caching backend if available. If false or no cachehandler exists,
   *   The webservice is invoked directly
   * @return
   *   array
   *     An array containing the data as provided by the webservice
   */
  public function execute($service, $method, $params, $cache = TRUE) {
    // hand over the execution to a cache system
    if ($cache && $this->cachehandler) {
      $cachedata = $this->cachehandler->get($service, $method, $params);
      if ($cachedata) {
        return $cachedata;
      }
      // else we just continue getting the data from the connector
    }

    if ($this->connector instanceof CollectiveAccessConnector) {
      $result = $this->connector->execute($service, $method, $params);
      if ($cache && $this->cachehandler) {
        $this->cachehandler->set($service, $method, $params, $result);
      }
      return $result;
    }
  }
}