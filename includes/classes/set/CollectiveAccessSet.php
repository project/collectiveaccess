<?php
// $Id$

/**
 * @file
 * class CollectiveAccessSet represents a CollectiveAccess set
 */

class CollectiveAccessSet implements ICollectiveAccessItem {
  private $set;

  public function __construct($set = NULL) {
    if (is_array($set)) {
      $this->set = $set;
    }
    else if (is_object($set)) {
      $this->set = (array) $set;
    }
  }

  /**
   * Magic method to set attributes
   */
  public function __get($name) {
    if (array_key_exists($name, $this->set)) {
      return $this->set[$name];
    }
  }

  /**
   * Magic method to get attributes
   */
  public function __set($name, $value) {
    $this->set[$name] = $value;
  }

  /**
   * Get the set info data
   */
  public function getData() {
    return $this->set;
  }

  /**
   * Get a list of set items
   *
   * @return
   *   array
   *     A list of items in this set
   */
  public function getSetItems() {
    if ($this->set['items']) {
      return $this->set['items'];
    }
  }
}