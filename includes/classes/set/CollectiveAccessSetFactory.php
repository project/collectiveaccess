<?php
// $Id$

/**
 * @file
 * Factory class to manage CollectiveAccess sets
 */
class CollectiveAccessSetFactory implements ICollectiveAccessFactory {
  private $setitem_factory;
  protected $ca_instance;

  public function __construct(CollectiveAccess $ca) {
    $this->ca_instance = $ca;
    $this->setitem_factory = new CollectiveAccessObjectFactory($ca);
  }

  /**
   * Provides the possibility to assign a CollectiveAccessFactory object
   */
  public function setSetItemFactory(ICollectiveAccessFactory $of) {
    if ($of instanceof ICollectiveAccessFactory) {
      $this->setitem_factory = $of;
    }
  }

  /**
   * Create a CollectiveAccessSet object
   * This object contains functionality to retrieve set items
   *
   * @param int $item_id
   *   The unique object identifier for an object, as known to the CollectiveAccess instance
   * @param array $options
   *   Provides options to manipulate how the object should be created
   * @param bool $cache
   *   Whether objects should be loaded from cache if possible
   * @return
   *   CollectiveAccessSet
   *     An object containing the set data
   */
  public function create($item_id, $options = array(), $cache = NULL) {
    // Merge in default options.
    $options += array(
      'get_items' => TRUE,
    );

    if ($this->ca_instance instanceof CollectiveAccess) {
      $set = CollectiveAccessItemInfo::getSet($this->ca_instance, $item_id, $cache);
      if ($set) {
        $set_obj = new CollectiveAccessSet($set);
        // Add localised labels
        $all_sets = $this->getSets($cache);
        $set_locale_info = $all_sets[$item_id];
        $set_obj->labels = $set_locale_info;
        // Add set items
        if ($options['get_items']) {
          $items = $this->getSetItems($set_obj->set_id, $cache);
          if ($items) {
            $item_arr = array();
            foreach ($items as $id => $item) {
              $item_arr['data'][$id] = $item->getData();
              $item_arr['objects'][$id] = $item;
            }
            $set_obj->items = $item_arr;
          }
        }
        return $set_obj;
      }
    }
    return FALSE;
  }

  /**
   * Get a list of available sets
   *
   * @param bool $cache
   *   Defines whether the data can come from cache if available
   * @return
   *   array
   *     A list of available sets
   */
  public function getSets($cache = NULL) {
    $sets = FALSE;
    if ($this->ca_instance instanceof CollectiveAccess) {
      $setsdata = CollectiveAccessItemInfo::getSets($this->ca_instance, $cache);
      foreach ($setsdata as $setid => $setinfo) {
        foreach ($setinfo as $localeid => $localeinfo) {
          $locale_name = $localeinfo['language'] . '_' . $localeinfo['country'];
          $sets[$setid][$locale_name] = $localeinfo;
        }
      }
    }
    return $sets;
  }

  /**
   * Get a list of available sets
   *
   * @param int $setid
   *   The unique identifier for a set, as known to the CollectiveAccess instance
   * @param bool $cache
   *   Defines whether the data can come from cache if available
   * @return
   *   array
   *     A list of available set items
   */
  public function getSetItems($setid, $cache) {
    $setitems = FALSE;
    if ($this->ca_instance instanceof CollectiveAccess) {
      $setitemsdata = CollectiveAccessItemInfo::getSetItems($this->ca_instance, $setid, $cache);
      foreach ($setitemsdata as $id => $iteminfoarr) {
        $iteminfo = array_shift($iteminfoarr);
        $item_id = $iteminfo['row_id'];
        $setitems[$item_id] = $this->setitem_factory->create($iteminfo['row_id']);
      }
    }
    return $setitems;
  }

}