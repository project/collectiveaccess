<?php

// $Id$

/**
 * @file
 * Interface for defining the basic structure of CollectiveAccess Factory classes
 */

interface ICollectiveAccessFactory {
  /**
   * Lets the factory class create an item instance
   */
  public function create($item_id, $options = array(), $cache = NULL);
}