<?php
// $Id$

/**
 * @file
 * Factory class to create CollectiveAccess collection objects
 *
 * Inherit this class to customize how your CollectiveAccess data
 * should be mapped to a CollectiveAccessObject
 */

class CollectiveAccessObjectFactory implements ICollectiveAccessFactory {
  protected $ca_instance;

  public function __construct(CollectiveAccess $ca) {
    if ($ca instanceof CollectiveAccess) {
      $this->ca_instance = $ca;
    }
  }

  /**
   * Create a full CollectiveAccess Object (as stored in ca_objects in CollectiveAccess)
   * This object contains, next to the basic CollectiveAccess object fields:
   *  - attributes
   *  - labels
   *  - images
   *  - relationships
   *
   * @param int $item_id
   *   The unique object identifier for an object, as known to the CollectiveAccess instance
   * @param array $options
   *   Provides options to manipulate how the object should be created
   * @param bool $cache
   *   Whether objects should be loaded from cache if possible
   * @return
   *   CollectiveAccessObject
   *     An object containing the data stored in CollectiveAccess for this object id
   */
  public function create($item_id, $options = array(), $cache = NULL) {
    if ($this->ca_instance instanceof CollectiveAccess) {
      $data = CollectiveAccessItemInfo::getItem($this->ca_instance, 'ca_objects', $item_id, $cache);
      if ($data) {
        $object = new CollectiveAccessObject($data);
        $this->attachObjectAttributes($object);
        $this->attachObjectLabels($object);
        $this->attachObjectImages($object);
        $this->attachObjectEntities($object);
        $this->updateListItems($object);
        return $object;
      }
    }
    return FALSE;
  }

  /**
   * Create a basic CollectiveAccess Object (as stored in ca_objects in CollectiveAccess)
   * This object contains the basic CollectiveAccess object fields
   *
   * @param int $object_id
   *   The unique object identifier for an object, as known to the CollectiveAccess instance
   * @param bool $cache
   *   Whether objects should be loaded from cache if possible
   * @return
   *   CollectiveAccessObject
   *     An object containing the data stored in CollectiveAccess for this object id
   */
  public function createBasicObject($object_id, $cache = NULL) {
    if ($this->ca_instance instanceof CollectiveAccess) {
      $data = CollectiveAccessItemInfo::getItem($this->ca_instance, 'ca_objects', $object_id, $cache);
      if ($data) {
        $object = new CollectiveAccessObject($data);
        return $object;
      }
    }
    return FALSE;
  }

  /**
   * Attach attributes to object
   *
   * @param CollectiveAccessObject object
   *   The object to attach the corresponding attributes to
   */
  private function attachObjectAttributes(&$object) {
    if ($this->ca_instance instanceof CollectiveAccess) {
      $attributes = CollectiveAccessItemInfo::getAttributes($this->ca_instance, 'ca_objects', $object->object_id);
      if ($attributes) {
        foreach ($attributes as $attrib_name => $attrib_value) {
          $object->$attrib_name = $attrib_value;
        }
      }
    }
  }

  /**
   * Attach labels to object
   *
   * @param CollectiveAccessObject object
   *   The object to attach the corresponding labels to
   */
  private function attachObjectLabels(&$object) {
    if ($this->ca_instance instanceof CollectiveAccess) {
      $labels = CollectiveAccessItemInfo::getLabels($this->ca_instance, 'ca_objects', $object->object_id, "all");
      if ($labels) {
        $object->labels = $labels;
      }
    }
  }

  /**
   * Attach images to object
   *
   * @param CollectiveAccessObject object
   *   The object to attach the corresponding images to
   */
  private function attachObjectImages(&$object) {
    if ($this->ca_instance instanceof CollectiveAccess) {
      $images = CollectiveAccessItemInfo::getObjectRepresentations($this->ca_instance, $object->object_id);
      if ($images) {
        foreach ($images as $id => $imageinfo) {
          // get a list of image presets
          $presets = array_keys($imageinfo['urls']);
          if ($presets) {
            foreach ($presets as $preset) {
              $imgarr[$preset] = array(
                'tag' => $imageinfo['tags'][$preset],
                'url' => $imageinfo['urls'][$preset],
                'info' => $imageinfo['info'][$preset],
                'dimensions' => $imageinfo['dimensions'][$preset],
                'path' => $imageinfo['paths'][$preset],
              );
            }

            // add image info to object, grouped by preset
            if ($imageinfo['is_primary'] == '1') {
              $object->primary_image = $imgarr;
            }
            else {
              $object->more_images[] = $imgarr;
            }
          }
        }
      }
    }
  }

  /**
   * Attach entity relationships to object
   *
   * @param CollectiveAccessObject object
   *   The object to attach the corresponding relationships to
   */
  private function attachObjectEntities(&$object) {
    if ($this->ca_instance instanceof CollectiveAccess) {
      $entities = CollectiveAccessItemInfo::getRelationships($this->ca_instance, 'ca_objects', $object->object_id, "ca_entities");
      if ($entities) {
        $object->entities = $entities;
      }
    }
  }

  /**
   * Replace existing list item IDs with their human readable counterpart
   *
   * @param CollectiveAccessObject object
   *   The object to update the list items for
   */
  private function updateListItems(&$object) {
    if ($this->ca_instance instanceof CollectiveAccess) {
      if ($object->type_id) {
        $item = CollectiveAccessItemInfo::getItem($this->ca_instance, 'ca_list_items', $object->type_id);
        if ($item) {
          $object->type = $item['item_value'];
        }
      }
    }
  }
}