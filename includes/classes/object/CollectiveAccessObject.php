<?php
// $Id$

/**
 * @file
 * class CollectiveAccessObject represents a CollectiveAccess collection object
 */

class CollectiveAccessObject implements ICollectiveAccessItem {
  private $data;

  public function __construct($data = NULL) {
    if (is_array($data)) {
      $this->data = $data;
    }
    else if (is_object($data)) {
      $this->data = (array) $data;
    }
  }

  public function __get($name) {
    if (array_key_exists($name, $this->data)) {
      return $this->data[$name];
    }
  }

  public function __set($name, $value) {
    $this->data[$name] = $value;
  }

  public function getData() {
    return $this->data;
  }

  public function getAttributes() {
    if (is_array($this->data)) {
      return array_keys($this->data);
    }
    return FALSE;
  }
}