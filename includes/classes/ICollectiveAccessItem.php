<?php

// $Id$

/**
 * @file
 * Interface for defining the basic structure of a CollectiveAccess item class
 */

interface ICollectiveAccessItem {
  /**
   * Provide a constructor, optionally passing in item data
   */
  public function __construct($data = NULL);

  /**
   * Provide a magic method to get attributes
   */
  public function __get($name);

  /**
   * Provide a magic method to set attributes
   */
  public function __set($name, $value);

  /**
   * Provide a method to retrieve object data
   */
  public function getData();
}