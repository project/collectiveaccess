<?php
// $Id$

/**
 * @file
 *
 * SOAP webservice implementation class for a connector to a CollectiveAccess installation
 */

class CollectiveAccessSOAPConnector extends CollectiveAccessConnector {
  private $service_clients;
  private $error;

  /**
   * Construct SOAP connector
   *
   * @param $settings: array containing SOAP settings.
   * keys:
   *   - service_path: full URL to CollectiveAccess service.php
   */
  public function __construct($settings) {
    if (is_array($settings) && $settings['service_path']) {
      parent::__construct($settings);
    }
    else {
      $this->error = 'Incorrect settings supplied in CollectiveAccessSOAPConnector';
      watchdog('collectiveaccess', $this->error, array(), WATCHDOG_ERROR);
      return $this->error;
    }
  }

  /**
   * Connect to the SOAP webservice given the provided settings
   *
   * @param: service to call on remote webservice
   */
  private function connect($service) {
    $wsdl = $this->settings['service_path'] . '/' . strtolower($service) . '/' . $service . '/soapWSDL';
    $options = array();
    $options['use'] = 'encoded';
    $options['style'] = 'rpc';
    if (module_exists('soapclient')) {
      $client = soapclient_init_client($wsdl, 1, $options);
      if (!empty($client['#error'])) {
        $this->error = 'SOAP error: ' . $client['#error'];
        watchdog('collectiveaccess', $this->error, array(),  WATCHDOG_ERROR);
        $this->service_clients[$service] = FALSE;
      }
      else {
        $this->service_clients[$service] = $client['#return'];
      }
    }
    else {
      $this->error = 'Missing module: soapclient';
    }
  }

  /**
   * Initiate a SOAP client for a service, if not yet existing
   */
  private function service_init($service) {
    if (!$this->service_clients[$service] instanceof DrupalSoapClient) {
      $this->connect($service);
    }
  }

  /**
   * Execute a method on the remote SOAP service
   */
  public function execute($service, $method, $params) {
    $this->service_init($service);
    if ($this->service_clients[$service] instanceof DrupalSoapClient) {
      $result = $this->service_clients[$service]->call($method, $params);
      if ($result['#error']) {
        $this->error = 'SOAP execution error:' . $result['#error'];
        watchdog('collectiveaccess', $this->error, array(), WATCHDOG_ERROR);
      }
      if (is_array($result['#return'])) {
        return $result['#return'];
      }
    }
  }
}