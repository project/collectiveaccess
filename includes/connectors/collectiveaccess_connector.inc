<?php
// $Id$

/**
 * @file
 *
 * Generic class for a connector to a CollectiveAccess installation
 */

abstract class CollectiveAccessConnector {
  protected $settings;

  public function __construct($settings) {
    $this->settings = $settings;
  }

  /**
   * Make a connection to the CollectiveAccess API
   */
  //abstract function connect();

  /**
   * Execute an API function on the CollectiveAccess API
   */
  abstract function execute($service, $method, $params);
}