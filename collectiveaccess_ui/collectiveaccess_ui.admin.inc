<?php
// $Id$

/**
 * @file
 * Renders administrative pages for the collectiveaccess_ui module
 */

/**
 * Menu callback to render instance overview.
 */
function collectiveaccess_ui_admin_instances() {
  // Load all instances
  $instances = collectiveaccess_instance_settings_load();
  $header = array(t('Description'), t('Name'), t('State'), array('data' => t('Operations'), 'colspan' => 3));
  $rows = array();

  foreach ($instances as $key => $instance) {
    $state = '';
    $delete_revert_link = '';
    switch ($instance->export_type) {
      case 1:
        // "Normal" state.
        $state = '<em>' . t('Normal') . '</em>';
        $delete_revert_link = t('Delete');
        break;
      case 3:
        // "Overridden" state.
        $state = '<em>' . t('Overridden') . '</em>';
        $delete_revert_link = t('Revert');
        break;
      default:
        // "Default" state.
        $state = '<em>' . t('Default') . '</em>';
        $delete_revert_link = '';
        break;
    }

    $row = array();
    $row[] = check_plain($instance->description);
    $row[] = check_plain($instance->name);
    $row[] = $state;
    $row[] = l(t('Edit'), 'admin/settings/collectiveaccess/instances/edit/' . $instance->name);
    if ($delete_revert_link) {
      $row[] = l($delete_revert_link, 'admin/settings/collectiveaccess/instances/delete/' . $instance->name);
    }
    else {
      $row[] = '';
    }
    $row[] = l(t('Export'), 'admin/settings/collectiveaccess/instances/export/' . $instance->name);

    $rows[] = $row;
  }

  return theme('table', $header, $rows);
}


/**
 * Landing page to select CollectiveAccess connector
 */
function collectiveaccess_ui_admin_add_instance_select() {
  $output = '';
  $connectors = collectiveaccess_get_connectors();
  $items = array();
  if ($connectors) {
    foreach ($connectors as $key => $connector) {
      $items[] = l($connector['name'], 'admin/settings/collectiveaccess/instances/add/' . $key);
    }
  }
  $output .= t('Select how you want to connect to your CollectiveAccess instance');
  $output .= theme('item_list', $items);
  return $output;
}

/**
 * CollectiveAccess instance settings form.
 *
 * @param $name
 *   The instance's machine readable name.
 */
function collectiveaccess_ui_admin_instance($form_state, $param = NULL) {
  $connectors = collectiveaccess_get_connectors();

  // Check if we are updating or inserting an instance
  if (is_object($param)) {
    $instance = $param;
    drupal_set_title(t('@name settings', array('@name' => $instance->description)));
    $save_label = t('Update');
  }
  else {
    if ($connector = $connectors[$param]) {
      $instance = new stdClass;
      $instance->name = '';
      $instance->description = '';
      $instance->connector = $param;
      $instance->connector_settings = array();
      $save_label = t('Save');
    }
    else {
      drupal_set_message(t('Invalid connector type'), 'error');
      drupal_goto('admin/settings/collectiveaccess/instances');
      exit;
    }
  }

  $form = array();
  $form['#tree'] = TRUE;
  if (!empty($instance->caid)) {
    $form['caid'] = array(
      '#type' => 'value',
      '#value' => $instance->caid,
    );
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('CollectiveAccess instance name'),
    '#description' => t('The unique, machine readable name of this instance.'),
    '#default_value' => $instance->name,
    '#size' => 20,
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('CollectiveAccess instance description'),
    '#description' => t('The human readable name or description of this CollectiveAccess instance.'),
    '#default_value' => $instance->description,
    '#size' => 20,
    '#required' => TRUE,
  );

  $form['connector'] = array(
    '#type' => 'hidden',
    '#value' => $instance->connector,
  );

  $form['connector_info'] = array(
    '#type' => 'item',
    '#title' => t('Connector type'),
    '#value' => $instance->connector,
  );

  $form += collectiveaccess_ui_get_connector_settings_form($instance);

  // Items coming from code shouldn't have their name changed
  if (isset($instance->export_type)) {
    switch ($instance->export_type) {
      case 2:
        // "Default" state.
        $form['name']['#disabled'] = TRUE;
        $form['name']['#value'] = $form['name']['#default_value'];
        break;
      case 3:
        // "Overridden" state.
        $form['name']['#disabled'] = TRUE;
        $form['name']['#value'] = $form['name']['#default_value'];
        break;
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $save_label,
  );

  return $form;

}

/**
 * Validate the instance add/edit form.
 */
function collectiveaccess_ui_admin_instance_validate($form, &$form_state) {
  // Check for illegal characters in instance names.
  if (preg_match('/[^0-9a-zA-Z_-]/', $form_state['values']['name'])) {
    form_set_error('name', t('Please only use alphanumeric characters, underscores (_), and hyphens (-) for instance names.'));
  }
}

/**
 * Submit handler to save an instance
 */
function collectiveaccess_ui_admin_instance_submit($form, &$form_state) {
  $res = collectiveaccess_instance_settings_save($form_state['values']);
  if ($res) {
    drupal_set_message(t('Saved instance %instance.', array('%instance' => $form_state['values']['description'])));
  }
  $form_state['redirect'] = array('admin/settings/collectiveaccess/instances');
}

/**
 * Delete instance form.
 *
 * @param $name
 *   The instance's machine readable name.
 */
function collectiveaccess_ui_admin_instance_delete($form_state, $instance) {
  $form = array();

  $form['caid'] = array(
    '#type' => 'value',
    '#value' => $instance->caid,
  );
  $form['instance_description'] = array(
    '#type' => 'value',
    '#value' => $instance->description,
  );
  $form['export_type'] = array(
    '#type' => 'value',
    '#value' => $instance->export_type,
  );

  // Adjust the UI texts depending on export type.
  switch ($instance->export_type) {
    case 1:
      // "Normal" state.
      $message = t('Are you sure you want to delete %title?', array('%title' => $instance->description));
      $button = t('Delete');
      break;
    case 3:
      // "Overridden" state.
      $message = t('Are you sure you want to revert %title?', array('%title' => $instance->description));
      $button = t('Revert');
      break;
    default:
      // There are no other export types that can be deleted.
      drupal_goto('admin/settings/collectiveaccess/instances');
      return array();
  }

  return confirm_form($form,
    $message,
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/collectiveaccess/instances',
    t('This action cannot be undone.'),
    $button,
    t('Cancel')
  );

}

/**
 * Submit handler to delete a preset.
 */
function collectiveaccess_ui_admin_instance_delete_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    collectiveaccess_instance_settings_delete($form_state['values']['caid']);
    // Adjust message depending on export type.
    $message = $form_state['values']['export_type'] == 1 ?
      'Deleted instance %instance.' :
      'Reverted instance %instance.';
    drupal_set_message(t($message, array('%instance' => $form_state['values']['instance_description'])));
  }
  $form_state['redirect'] = 'admin/settings/collectiveaccess/instances';
}

/**
 * Export a CollectiveAccess instance.
 *
 * @param $instance
 *   An instance object.
 */
function collectiveaccess_ui_admin_instance_export($form_state, $instance) {
  drupal_set_title(t('Export of instance %description', array('%description' => $instance->description)));
  $form = array();

  $export = ctools_export_object('collectiveaccess_instance', $instance);
  $export = "<?php\n{$export}return \$instance;\n?>";

  // Create the export code textarea.
  ctools_include('export');
  $form['export'] = array(
    '#type' => 'textarea',
    '#title' => t('Instance code'),
    '#rows' => 20,
    '#default_value' => $export,
  );

  return $form;
}

/**
 * Import CollectiveAccess instance
 */
function collectiveaccess_ui_admin_instance_import() {
  $form = array();

  $form['import'] = array(
    '#type' => 'textarea',
    '#rows' => 10,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import')
  );

  return $form;
}

/**
 * Validates an instance import.
 */
function collectiveaccess_ui_admin_instance_import_validate($form, &$form_state) {
  // Run the import code, which should return an instance object.
  $instance = drupal_eval($form_state['values']['import']);
  if (empty($instance) || !is_object($instance) || empty($instance->name)) {
    form_set_error('import', t('The submitted instance code could not be interpreted.'));
  }
  elseif (collectiveaccess_instance_settings_load($instance->name)) {
    form_set_error('import', t('An instance with that name already exists.'));
  }
  else {
    // Pass the parsed object on to the submit handler.
    $form_state['values']['import_parsed'] = $instance;
  }
}

/**
 * Submit handler to import an instance.
 */
function collectiveaccess_ui_admin_instance_import_submit($form, &$form_state) {
  $instance = (array) $form_state['values']['import_parsed'];

  if (collectiveaccess_instance_settings_save($instance)) {
    drupal_set_message(t('Imported instance %instance.', array('%instance' => $instance['description'])));
  }
  else {
    drupal_set_message(t('Failed to import the instance.'), 'warning');
  }

  $form_state['redirect'] = 'admin/settings/collectiveaccess/instances';
}
